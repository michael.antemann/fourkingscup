import { Component, OnInit } from '@angular/core';
import { THEMES } from 'src/app/core/constants';
import { CoreService } from 'src/app/core/core.service';
@Component({
  selector: 'app-theme-select',
  templateUrl: './theme-select.component.html',
  styleUrls: ['./theme-select.component.scss'],
})
export class ThemeSelectComponent implements OnInit {
  themes = THEMES;

  theme$ = this.coreService.theme$;

  constructor(private coreService: CoreService) {}

  ngOnInit() {}

  setTheme(theme: string) {
    this.coreService.setTheme(theme);
  }
}

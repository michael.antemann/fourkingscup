import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { BgSelectComponent } from './bg-select/bg-select.component';
import { LanguageSelectComponent } from './language-select/language-select.component';
import { SettingsComponent } from './settings.component';
import { ThemeSelectComponent } from './theme-select/theme-select.component';

@NgModule({
  imports: [
    SharedModule,
    RouterModule,
    MatButtonModule,
    MatMenuModule,
    MatCardModule,
    MatIconModule,
  ],
  declarations: [
    SettingsComponent,
    LanguageSelectComponent,
    ThemeSelectComponent,
    BgSelectComponent,
  ],
})
export class SettingsModule {}

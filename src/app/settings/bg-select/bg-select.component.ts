import { Component, OnInit } from '@angular/core';
import { BACKGROUNDS } from 'src/app/core/constants';
import { CoreService } from 'src/app/core/core.service';

@Component({
  selector: 'app-bg-select',
  templateUrl: './bg-select.component.html',
  styleUrls: ['./bg-select.component.scss'],
})
export class BgSelectComponent implements OnInit {
  background$ = this.coreService.background$;
  backgrounds = BACKGROUNDS;
  constructor(private coreService: CoreService) {}

  ngOnInit() {}

  selectBg(bg: string) {
    this.coreService.setBackground(bg);
  }
}

import { Component, OnInit } from '@angular/core';
import { LANGUAGES } from 'src/app/core/constants';
import { CoreService } from 'src/app/core/core.service';
@Component({
  selector: 'app-language-select',
  templateUrl: './language-select.component.html',
  styleUrls: ['./language-select.component.scss'],
})
export class LanguageSelectComponent implements OnInit {
  languages = LANGUAGES;
  language$ = this.coreService.language$;
  constructor(private coreService: CoreService) {}

  ngOnInit() {}

  setLanguage(language: string) {
    this.coreService.setLanguage(language);
  }
}

import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { ChangeDetectionStrategy, Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { map, shareReplay } from 'rxjs/operators';
import { SidenavService } from './core/sidenav/sidenav.service';
import { CoreService } from './core/core.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit {
  backgroundSrc$ = this.coreService.backgroundSrc$;

  @ViewChild('sidenav', { static: true })
  sidenav: MatSidenav;

  isHandset$ = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map((result) => result.matches),
    shareReplay()
  );

  constructor(
    private sidenavService: SidenavService,
    private breakpointObserver: BreakpointObserver,
    private coreService: CoreService
  ) {}

  ngOnInit() {
    this.coreService.initialize();
    this.sidenavService.setSidenav('main', this.sidenav);
  }
}

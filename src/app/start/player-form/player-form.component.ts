import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-player-form',
  templateUrl: './player-form.component.html',
  styleUrls: ['./player-form.component.scss']
})
export class PlayerFormComponent implements OnInit {
  @Input()
  form: FormGroup;

  @Output()
  add = new EventEmitter();

  @Output() // ------------ player index
  removePlayer: EventEmitter<number> = new EventEmitter();
  constructor() {}

  ngOnInit() {}
}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { debounceTime, takeUntil, tap } from 'rxjs/operators';
import { GameService } from 'src/app/game/game.service';
@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss'],
})
export class StartComponent implements OnInit, OnDestroy {
  form = this.fb.group({
    players: this.fb.array(['', '']),
  });

  unsubscribe$ = new Subject<boolean>();

  constructor(private fb: FormBuilder, private gameService: GameService) {}

  ngOnInit() {
    this.initPlayers();
    this.form.valueChanges
      .pipe(
        takeUntil(this.unsubscribe$),
        debounceTime(300),
        tap((value) => localStorage.setItem('playersForm', JSON.stringify(value)))
      )
      .subscribe();
  }

  initPlayers(): void {
    const initValue = JSON.parse(localStorage.getItem('playersForm'));
    if (initValue?.players?.length) {
      this.players.clear();
      for (const player of initValue.players) {
        this.players.push(this.fb.control(player));
      }
    }
  }

  get players(): FormArray {
    return this.form.get('players') as FormArray;
  }

  addPlayer() {
    this.players.push(this.fb.control('', [Validators.required]));
  }

  removePlayer(index: number) {
    this.players.removeAt(index);
  }

  startGame() {
    this.gameService.newGame(this.players.value);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next(true);
    this.unsubscribe$.complete();
  }
}

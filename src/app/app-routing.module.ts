import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GameComponent } from './game/game.component';
import { HasPlayersGuard } from './game/has-players.guard';
import { RulesComponent } from './rules/rules.component';
import { SettingsComponent } from './settings/settings.component';
import { StartComponent } from './start/start.component';

const routes: Routes = [
  {
    path: '',
    component: StartComponent,
  },
  {
    path: 'game',
    canActivate: [HasPlayersGuard],
    component: GameComponent,
  },
  {
    path: 'rules',
    children: [
      {
        path: '',
        component: RulesComponent,
      },
      {
        path: ':rulesetId',
        component: RulesComponent,
      },
    ],
  },
  {
    path: 'settings',
    component: SettingsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

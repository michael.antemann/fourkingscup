export const LANGUAGES = { de: 'GERMAN', en: 'ENGLISH' };
export const THEMES = ['dark', 'light'];
export const BACKGROUNDS = ['bg1', 'bg2', 'bg3', 'bg4', 'bg5', 'bg6', 'bg7', 'bg8'];

export const TENOR = {
  url: 'https://api.tenor.com/v1/random',
  apiKey: 'P1GRJDDWA6IU',
};

export interface NavigationItem {
  url: string;
  translate: string;
  adminOnly?: boolean;
}

export const NAVIGATION: NavigationItem[] = [
  { url: '/', translate: 'NEW_GAME' },
  { url: '/rules', translate: 'RULES' },
  { url: '/settings', translate: 'SETTINGS' },
];

export const CARDS_PER_COLOR = 13;
export const [DEFAULT_LANGUAGE] = Object.keys(LANGUAGES);
export const [DEFAULT_THEME] = THEMES;
export const [DEFAULT_BACKGROUND] = BACKGROUNDS;

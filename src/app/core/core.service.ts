import { DOCUMENT } from '@angular/common';
import { Inject, Injectable, NgZone, Renderer2, RendererFactory2 } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  DEFAULT_BACKGROUND,
  DEFAULT_LANGUAGE,
  DEFAULT_THEME,
  NAVIGATION,
  THEMES,
} from './constants';

@Injectable({ providedIn: 'root' })
export class CoreService {
  private renderer: Renderer2;

  constructor(
    rendererFactory: RendererFactory2,
    @Inject(DOCUMENT) private document: Document,
    private translate: TranslateService
  ) {
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  private _language = new BehaviorSubject<string>(DEFAULT_LANGUAGE);
  readonly language$ = this._language.asObservable();

  private _theme = new BehaviorSubject<string>(DEFAULT_THEME);
  readonly theme$ = this._theme.asObservable();

  readonly navigation$ = of(NAVIGATION);

  private _background = new BehaviorSubject<string>(DEFAULT_BACKGROUND);
  readonly background$ = this._background.asObservable();
  readonly backgroundSrc$ = this.background$.pipe(
    map((background) => 'assets/images/bg/' + background + '.jpg')
  );

  initialize(): void {
    this.setLanguage(localStorage.getItem('language'));
    this.setTheme(localStorage.getItem('theme'));
    this.setBackground(localStorage.getItem('background'));
  }

  setBackground(background: string): void {
    background = background || DEFAULT_BACKGROUND;
    localStorage.setItem('background', background);
    this._background.next(background);
  }

  setTheme(theme): void {
    theme = theme || DEFAULT_THEME;
    for (const possibleTheme of THEMES) {
      this.renderer.removeClass(this.document.body, this.getThemeClass(possibleTheme));
    }
    this.renderer.addClass(this.document.body, this.getThemeClass(theme));
    localStorage.setItem('theme', theme);
    this._theme.next(theme);
  }

  setLanguage(language: string): void {
    language = language || DEFAULT_LANGUAGE;
    this.translate.setDefaultLang(language);
    this.translate.use(language);
    localStorage.setItem('language', language);
    this._language.next(language);
  }

  getThemeClass(name: string) {
    return `${name}-theme`;
  }
}

import { MatSidenav } from '@angular/material/sidenav';

export class SidenavRegistry {
  [key: string]: MatSidenav;
}

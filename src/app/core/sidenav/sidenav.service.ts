import { Injectable } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { SidenavRegistry } from './sidenav.registry';

@Injectable({ providedIn: 'root' })
export class SidenavService {
  constructor() {}

  private sidenavRegistry: SidenavRegistry = {};

  setSidenav(name: string, sidenav: MatSidenav) {
    this.sidenavRegistry = {
      ...this.sidenavRegistry,
      [name]: sidenav
    };
  }

  get(name: string): MatSidenav {
    return this.sidenavRegistry[name];
  }
}

import { Component } from '@angular/core';
import { CoreService } from '../core.service';
import { SidenavService } from './sidenav.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
})
export class SidenavComponent {
  constructor(private sidenavService: SidenavService, private coreService: CoreService) {}

  navigation$ = this.coreService.navigation$;

  close() {
    this.sidenavService.get('main').close();
  }
}

import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { filter, map, take, tap } from 'rxjs/operators';
import { FinishDialogComponent } from './finish-dialog/finish-dialog.component';
import { Card } from './models/card';
import { CardUtils } from './card-utils';

let INIT_DECK = CardUtils.generateDeck();
const INIT_ACTIVE_CARD = CardUtils.getRandom(INIT_DECK);
INIT_DECK = INIT_DECK.filter(
  (card) => card.no !== INIT_ACTIVE_CARD.no && card.symbol !== INIT_ACTIVE_CARD.symbol
);

@Injectable({ providedIn: 'root' })
export class GameService {
  constructor(private router: Router, private dialog: MatDialog) {}
  // Players
  private _players = new BehaviorSubject<string[]>([]);
  readonly players$ = this._players.asObservable();
  readonly hasPlayers$ = this.players$.pipe(map((players) => players.length >= 2));
  // Active Player
  private _activePlayerIndex = new BehaviorSubject<number>(0);
  readonly activePlayerIndex$ = this._activePlayerIndex.asObservable();
  readonly activePlayer$ = this.determineActivePlayer();
  // King Counter
  private _kingCounter = new BehaviorSubject<number>(0);
  readonly kingCounter$ = this._kingCounter.asObservable();
  readonly kings$ = this.kingCounter$.pipe(map((kingCounter) => Array(kingCounter).fill(1)));
  // Deck
  private _deck = new BehaviorSubject<Card[]>(INIT_DECK);
  readonly deck$ = this._deck.asObservable();
  readonly remainingCards$ = this.deck$.pipe(map((deck) => deck.length));
  readonly hasCards$ = this.remainingCards$.pipe(map((remainingCards) => remainingCards > 0));
  // Active Card
  private _activeCard = new BehaviorSubject<Card>(INIT_ACTIVE_CARD);
  readonly activeCard$ = this._activeCard.asObservable();
  readonly hasActiveCard$ = this.activeCard$.pipe(map((card) => Object.keys(card).length > 0));

  setPlayers(players: string[]): void {
    if (players.length > 0) {
      this._players.next(players);
      this._activePlayerIndex.next(0);
    }
  }

  nextPlayer(): void {
    const currentPlayerIndex = this._activePlayerIndex.value;
    const MAX_INDEX = this._players.value.length - 1;
    const nextPlayerIndex = currentPlayerIndex === MAX_INDEX ? 0 : currentPlayerIndex + 1;
    this._activePlayerIndex.next(nextPlayerIndex);
  }

  determineActivePlayer(): Observable<string> {
    return combineLatest([this.players$, this.activePlayerIndex$]).pipe(
      map(([players, index]) => {
        if (players.length <= index) {
          return null;
        }
        return players[index];
      })
    );
  }

  newDeck(): void {
    const newDeck = CardUtils.generateDeck();
    this._deck.next(newDeck);
    this._kingCounter.next(0);
  }

  drawCard(): void {
    // current deck
    let deck = this._deck.value;
    // draw card from it
    const newActiveCard = CardUtils.getRandom(deck);
    this._activeCard.next(newActiveCard);
    // filter new card out of deck
    deck = deck.filter((card) => this.filterByActiveCard(card, newActiveCard));
    // if deck if now empty, generate a new, otherwise refresh deck (substract new card)
    deck.length === 0 ? this.newDeck() : this._deck.next(deck);
    // check if a king was drawn
    this.checkForKing(newActiveCard.no);
    // set next player
    this.nextPlayer();
  }

  checkForKing(cardNo: number): void {
    const isKing = cardNo === 13;
    if (isKing) {
      const newKingCounter = this._kingCounter.value + 1;
      this._kingCounter.next(newKingCounter);
      const MAX = 4;
      if (newKingCounter === MAX) {
        const dialogRef = this.dialog.open(FinishDialogComponent, {
          disableClose: true,
        });

        dialogRef
          .afterClosed()
          .pipe(
            filter((newDeck) => newDeck),
            tap(() => this.newDeck())
          )
          .subscribe();
      }
    }
  }

  newGame(players: string[]): void {
    if (players) {
      this.setPlayers(players);
    }
    this.newDeck();
    this.drawCard();
    this.router.navigate(['/game']);
  }

  filterByActiveCard = (card: Card, activeCard: Card): boolean =>
    card.no !== activeCard.no || card.symbol !== activeCard.symbol;
}

import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { CardTextComponent } from './card-text/card-text.component';
import { CardComponent } from './card/card.component';
import { FinishDialogComponent } from './finish-dialog/finish-dialog.component';
import { GameComponent } from './game.component';
import { KingCounterComponent } from './king-counter/king-counter.component';
import { PlayerBoardComponent } from './player-board/player-board.component';
import { MatChipsModule } from '@angular/material/chips';
@NgModule({
  imports: [
    SharedModule,
    RouterModule,
    MatCardModule,
    MatDialogModule,
    MatChipsModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatButtonModule,
  ],
  declarations: [
    GameComponent,
    CardComponent,
    CardTextComponent,
    PlayerBoardComponent,
    KingCounterComponent,
    FinishDialogComponent,
  ],
})
export class GameModule {}

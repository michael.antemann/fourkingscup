import { Component, OnInit } from '@angular/core';
import { GameService } from './game.service';
@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent implements OnInit {
  activeCard$ = this.gameService.activeCard$;
  players$ = this.gameService.players$;
  remainingCards$ = this.gameService.remainingCards$;
  activePlayerIndex$ = this.gameService.activePlayerIndex$;
  kings$ = this.gameService.kings$;
  constructor(private gameService: GameService) {}

  ngOnInit() {}

  drawCard() {
    this.gameService.drawCard();
  }
}

import { ApplicationRef, Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FinishDialogService } from './finish-dialog.service';

@Component({
  selector: 'app-finish-dialog',
  templateUrl: './finish-dialog.component.html',
  styleUrls: ['./finish-dialog.component.scss']
})
export class FinishDialogComponent implements OnInit {
  gifUrl$ = this.finishDialogService.getRandomGifUrl();
  constructor(
    private applicationRef: ApplicationRef,
    private dialogRef: MatDialogRef<FinishDialogComponent>,
    private finishDialogService: FinishDialogService
  ) {}

  ngOnInit() {}
  /**
   * Dialog will be opened within GameState
   * will somehow run outside angular zone
   */
  close(result?: boolean) {
    this.dialogRef.close(result);
    setTimeout(() => {
      this.applicationRef.tick();
    });
  }
}

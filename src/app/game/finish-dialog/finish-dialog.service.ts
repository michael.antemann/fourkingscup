import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TENOR } from 'src/app/core/constants';

@Injectable({ providedIn: 'root' })
export class FinishDialogService {
  constructor(private http: HttpClient) {}

  getRandomGifUrl(): Observable<string> {
    const params = new HttpParams().set('limit', '1').set('key', TENOR.apiKey).set('q', 'panda');
    return this.http.get<any>(TENOR.url, { params }).pipe(
      map((response) => response.results),
      map((results) => {
        if (!results || results.length === 0) {
          return '';
        }
        const [result] = results;

        const noMedia = !result.media || result.media.length === 0;
        if (noMedia) {
          return '';
        }
        const [mediaObject] = result.media;
        const { gif } = mediaObject;
        if (!gif) {
          return '';
        }

        return gif.url;
      })
    );
  }
}

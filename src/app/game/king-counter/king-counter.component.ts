import { Component, Input, OnInit } from '@angular/core';
import {
  bounceInOnEnterAnimation,
  bounceOutOnLeaveAnimation
} from 'angular-animations';

@Component({
  selector: 'app-king-counter',
  templateUrl: './king-counter.component.html',
  styleUrls: ['./king-counter.component.scss'],
  animations: [bounceInOnEnterAnimation(), bounceOutOnLeaveAnimation()]
})
export class KingCounterComponent implements OnInit {
  @Input()
  kings: any[];

  constructor() {}

  ngOnInit() {}
}

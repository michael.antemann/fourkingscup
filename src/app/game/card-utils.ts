import { CARDS_PER_COLOR } from 'src/app/core/constants';
import { Card, CardSymbol } from './models/card';

export class CardUtils {
  static generateDeck(): Card[] {
    const cards = [];
    for (const symbol in CardSymbol) {
      if (CardSymbol.hasOwnProperty(symbol)) {
        /**
         * 2 = 2, 3 = 3, ...
         * 11 = jack, ...
         * 14 = ass
         */
        for (let no = 1; no <= CARDS_PER_COLOR; no++) {
          const card = new Card(CardSymbol[symbol as CardSymbol], no);
          cards.push(card);
        }
      }
    }
    return [...cards];
  }

  static getRandom(deck: Card[]): Card {
    const index = Math.floor(Math.random() * deck.length);
    return { ...deck[index] };
  }
}

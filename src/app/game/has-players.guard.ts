import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { interval } from 'rxjs';
import { tap, timeout } from 'rxjs/operators';
import { GameService } from './game.service';

@Injectable({ providedIn: 'root' })
export class HasPlayersGuard implements CanActivate {
  constructor(private gameService: GameService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.gameService.hasPlayers$.pipe(
      tap((hasPlayers) => {
        console.log(hasPlayers);
        if (!hasPlayers) {
          this.router.navigate(['/']);
        }
      })
    );
  }
}

import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-player-board',
  templateUrl: './player-board.component.html',
  styleUrls: ['./player-board.component.scss']
})
export class PlayerBoardComponent implements OnInit {
  @Input()
  players: string[];

  @Input()
  activeIndex: number;

  constructor() {}

  ngOnInit() {}
}

import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { fadeInOnEnterAnimation, fadeOutOnLeaveAnimation } from 'angular-animations';
import { Card } from '../models/card';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  animations: [fadeInOnEnterAnimation(), fadeOutOnLeaveAnimation()],
})
export class CardComponent implements OnInit, OnChanges {
  @Input()
  card: Card;

  cardSrc: string;

  constructor() {}

  ngOnInit() {}

  ngOnChanges(): void {
    const base = '/assets/images/flat-playing-cards';
    this.cardSrc = `${base}/${this.card.symbol}/${this.card.no}.png`;
  }
}

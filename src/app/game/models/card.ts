export enum CardSymbol {
  heart = 'heart', // ♥ Herz
  spade = 'spade', // ♠ Pik
  club = 'club', // ♣ Kreuz
  diamond = 'diamond' // ♦ Karo
}

export class Card {
  constructor(public symbol: CardSymbol, public no: number) {}
}

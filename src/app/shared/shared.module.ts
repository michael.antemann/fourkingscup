import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { LogoXLComponent } from './logo-xl/logo-xl.component';
import { LogoComponent } from './logo/logo.component';

const modules = [CommonModule, FlexLayoutModule, ReactiveFormsModule, TranslateModule];

@NgModule({
  declarations: [LogoComponent, LogoXLComponent],
  imports: [...modules],
  exports: [...modules, LogoComponent, LogoXLComponent],
  providers: [],
})
export class SharedModule {}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-logo-xl',
  templateUrl: './panda2.svg',
  styleUrls: ['./logo-xl.component.scss'],
})
export class LogoXLComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.scss'],
})
export class RulesComponent implements OnInit {
  rulesetForm = this.fb.array([]);

  constructor(private fb: FormBuilder) {}

  ngOnInit() {}
}

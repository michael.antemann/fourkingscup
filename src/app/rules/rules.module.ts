import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { RuleDetailComponent } from './rule-detail/rule-detail.component';
import { RulesListItemComponent } from './rules-list/rules-list-item/rules-list-item.component';
import { RulesListComponent } from './rules-list/rules-list.component';
import { RulesComponent } from './rules.component';
import { RulesetDetailComponent } from './ruleset-detail/ruleset-detail.component';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  imports: [
    SharedModule,
    RouterModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
  ],
  declarations: [
    RulesComponent,
    RulesetDetailComponent,
    RuleDetailComponent,
    RulesListComponent,
    RulesListItemComponent,
  ],
})
export class RulesModule {}

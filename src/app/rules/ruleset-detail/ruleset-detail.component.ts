import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-ruleset-detail',
  templateUrl: './ruleset-detail.component.html',
  styleUrls: ['./ruleset-detail.component.scss'],
})
export class RulesetDetailComponent implements OnInit {
  @Input()
  ruleset: any;

  @Input()
  rulesetForm: FormArray;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.rulesetForm.push(this.fb.control(this.ruleset.name));
    for (const rule of this.ruleset.rules) {
      const { cardNo, text, title } = rule;
      this.rulesetForm.push(
        this.fb.group({
          cardNo,
          text,
          title,
        })
      );
    }
  }

  saveRuleset(): void {
    const [name] = this.rulesetForm.value;
    const rules = this.rulesetForm.value.filter((item) => typeof item !== 'string');
    alert('NEEDS IMPLEMENTATION');
  }
}

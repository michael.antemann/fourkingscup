export class NgxsRulesetFormState {
  model: any;
  dirty: boolean;
  status: string;
  errors: any;
  constructor() {
    this.model = [];
    this.dirty = false;
    this.status = '';
    this.errors = {};
  }
}

import { CARDS_PER_COLOR } from '../core/constants';

export class RulesUtils {
  static generateEmptyRuleset(): any {
    const ruleset = {
      id: '',
      name: '',
      rules: RulesUtils.generateEmptyRules(),
    };
    return ruleset;
  }

  static generateEmptyRules(): any[] {
    const rules = [];
    for (let i = 1; i <= CARDS_PER_COLOR; i++) {
      rules.push({
        cardNo: i,
        text: '',
        title: '',
      });
    }
    return rules;
  }
}

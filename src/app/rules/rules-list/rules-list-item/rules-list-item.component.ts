import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-rules-list-item',
  templateUrl: './rules-list-item.component.html',
  styleUrls: ['./rules-list-item.component.scss'],
})
export class RulesListItemComponent implements OnInit {
  @Input()
  ruleset: any;

  constructor() {}

  ngOnInit() {}
}

import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-rules-list',
  templateUrl: './rules-list.component.html',
  styleUrls: ['./rules-list.component.scss'],
})
export class RulesListComponent implements OnInit {
  @Input()
  rulesets: any[];
  constructor() {}

  ngOnInit() {}
}
